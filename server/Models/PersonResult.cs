using System.Collections.Generic;

namespace Server.Models
{
    public class PersonResult
    {
        public List<Person> Persons { get; set; }
        public int TotalRecords { get; set; }
    }
}