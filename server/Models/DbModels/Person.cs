using System.Collections.Generic;

namespace Server.Models
{
    public class Person
    {
        public Person()
        {
            Nums = new List<Num>();
        }
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public ICollection<Num> Nums { get; set; }
        
    }
}