namespace Server.Models
{
    public class Num
    {
        public int NumId { get; set; }
        public string Number { get; set; }
        public int PersonId { get; set; }
    }
}