using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Models;

public interface IAppRepository
{
    Task<PersonResult> GetAllPersons(int skip, int take);
    Task CreatePerson(string firstName, string lastName, string address);
    Task CreateNum(string number, string userId);
    Task<Person> FindPersonByName(string name);
    void InitProcedures();
    Task<PersonResult> GetPersonsByName(string q, int skip, int take);
}