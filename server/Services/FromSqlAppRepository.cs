using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Server.Data;
using Server.Models;

namespace Server.Services
{
    public class FromSqlAppRepository : IAppRepository
    {
        private AppDbContext _context;
        public const int recordsPerPage = 50;

        public FromSqlAppRepository(AppDbContext context)
        {
            _context = context;
        }

        public void InitProcedures()
        {
            _context.Database.ExecuteSqlCommand(@"
                CREATE PROC CreatePerson 
                @FirstName nvarchar(max),
                @LastName nvarchar(max),
                @Address nvarchar(max)
                AS
                INSERT INTO dbo.Persons (FirstName, LastName, Address)
                VALUES (@FirstName, @LastName, @Address)"
            );

            _context.Database.ExecuteSqlCommand(@"
                CREATE PROC CreateNum
                @Number nvarchar(max),
                @PersonId int = NULL
                AS
                INSERT INTO dbo.Nums (Number, PersonId)
                VALUES (@Number, @PersonId)
            ");

            _context.Database.ExecuteSqlCommand(@"
                CREATE PROC GetAllPersons
                AS
                SELECT * 
                FROM dbo.Persons
            ");

            _context.Database.ExecuteSqlCommand(@"
                CREATE PROC FindPersonByName
                @Name nvarchar(max)
                AS
                SELECT *
                FROM dbo.Persons
                WHERE 
                FirstName + ' ' + LastName LIKE @Name + '%' OR 
                LastName + ' ' + FirstName LIKE @Name + '%' OR 
                Address LIKE @Name + '%'
            ");

            _context.Database.ExecuteSqlCommand(@"
                CREATE PROC GetNumsForPerson
                @PersonId int
                AS
                SELECT *
                FROM dbo.Nums
                WHERE @PersonId = PersonId
            ");
        }

        public async Task CreateNum(string number, string PersonId = null)
        {
            await _context.Database.ExecuteSqlCommandAsync($"EXEC dbo.CreateNum @Number='{number}', @PersonID='{PersonId}'");
        }

        public async Task CreatePerson(string firstName, string lastName, string address)
        {
            var command = $"EXEC dbo.CreatePerson @FirstName='{firstName}', @LastName='{lastName}', @Address='{address}'";
            await _context.Database.ExecuteSqlCommandAsync(command);
        }

        public async Task<Person> FindPersonByName(string name)
        {
            var search = $"EXEC dbo.FindPersonByName @Name='{name}'";
            return await _context.Persons.FromSql(search).FirstOrDefaultAsync();
        }

        public async Task<PersonResult> GetAllPersons(int skip, int take = recordsPerPage)
        {
            PersonResult result = new PersonResult();

            var personsQuery =  _context
                .Persons
                .FromSql("EXEC dbo.GetAllPersons");
            
            result.TotalRecords = await personsQuery.CountAsync();

            List<Person> persons;
            persons = await personsQuery.Skip( skip ).Take( take ).ToListAsync();

            foreach (var person in persons)
            {
                await _context.Nums.FromSql($"EXEC dbo.GetNumsForPerson @PersonId={person.PersonId}").ToListAsync();
            }

            result.Persons = persons;

            return result;
        }

        public async Task<PersonResult> GetPersonsByName(string q, int skip = 0, int take = recordsPerPage)
        {
            var result = new PersonResult();

            var personsQuery = _context
                .Persons
                .FromSql($"EXEC dbo.FindPersonByName @Name='{q}'");
            result.TotalRecords = await personsQuery.CountAsync();
            
            List<Person> persons;

            persons = await personsQuery.Skip( skip ).Take( take ).ToListAsync();

            foreach (var person in persons)
            {
                await _context.Nums.FromSql($"EXEC dbo.GetNumsForPerson @PersonId={person.PersonId}").ToListAsync();
            }
            result.Persons = persons;

            return result;
        }
    }
}