using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Models;
using Server.Services;

namespace Server.Controllers 
{
    public class NumsController : Controller
    {
        private IAppRepository _repository;

        public NumsController(IAppRepository repository)
        {
            _repository = repository;
        }

        public async Task<JsonResult> Get(string q, int skip, int take)
        {
            try 
            {
                var persons = await _repository.GetPersonsByName(q, skip, take);           
                return Json(new {
                    persons = persons.Persons,
                    totalRecords = persons.TotalRecords
                });
            }
            catch 
            {
                return Json(string.Empty);
            }
        }

        public async Task<JsonResult> AllPersons(int skip, int take)
        {
            try
            {
                var persons = await _repository.GetAllPersons(skip, take);
                return Json(new {
                    persons = persons.Persons,
                    totalRecords = persons.TotalRecords
                });
            }
            catch
            {
                return Json(string.Empty);
            }
        }
    }
}