﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Server.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Server.Services;

namespace Server
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env) 
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddDbContext<AppDbContext>(options => 
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddMvc();
            services.AddTransient<IAppRepository, FromSqlAppRepository>();
            services.AddSingleton<IDbInitializer, FromSqlDbInitializer>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, AppDbContext appDbContext, IDbInitializer dbInitializer)
        {
            // Create and populate database;
            dbInitializer.Initialize();
            // Setting up CORS for local dev server
            app.UseCors(builder => 
                builder.WithOrigins("http://localhost:3000", "http://localhost:5001", "http://192.168.0.95:3000", "http://192.168.0.95:5001")
            );
            // This is a fallback for SPA. If request isn't resovled,
            // we let react app to do so.
            app.Use(async (context, next) => {
                await next();
                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value)) 
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });
            app.UseStaticFiles();
            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "api",
                    template: "api/{controller=Nums}/{action=Get}/{id?}"
                );
            });
        }
    }
}
