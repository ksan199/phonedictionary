using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Server.Models;

namespace Server.Data
{
    public class FromSqlDbInitializer : IDbInitializer
    {
        private IAppRepository _repository;
        private AppDbContext _context;

        public FromSqlDbInitializer(IAppRepository repository, AppDbContext context)
        {
            _repository = repository;
            _context = context;
        }
        async void IDbInitializer.Initialize()
        {
            if (_context.Database.EnsureCreated())
            {
                _repository.InitProcedures();
                for (int i=0; i<500; i++) {
                    await _repository.CreatePerson("Common", "Name", "Russia, St. Petersburg");
                    await _repository.CreatePerson("thispersonhas", "verylongname", "France, Nantes");
                    await _repository.CreatePerson("Li", "Yeng", "China, Nanjing");
                }
                
                var ppl = new List<Person>[] {
                    _repository.GetPersonsByName("Common", 0, 500).Result.Persons,
                    _repository.GetPersonsByName("very", 0, 500).Result.Persons,
                    _repository.GetPersonsByName("Li", 0, 500).Result.Persons
                };

                foreach (var people in ppl)
                {
                    foreach (var person in people)
                    {
                        await _repository.CreateNum("89040199517", person.PersonId.ToString());
                        await _repository.CreateNum("89213447282", person.PersonId.ToString());
                    }      
                }
            }
            
        }
    }
}