import React from 'react';
import { render } from 'react-dom'
import './css/index.css';
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducers'
import Root from './Root'
import thunkMiddleware from 'redux-thunk'
import { recieveSavedNumbers } from './actions'

let store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
)

// Load saved tels from local store
store.dispatch(recieveSavedNumbers())

render(
    <Root store={store} />, 
    document.getElementById('root')
);

