import React from 'react'
import { Link } from 'react-router-dom'
import URLSearchParams from 'url-search-params'
import * as help from '../helpers'

const pagination = ({ totalPages, location, onPageClick }) => {
    let currentPage = parseInt((new URLSearchParams(location.search)).get('p'), 10)
    currentPage = help.normalizePage(currentPage)
    return (
        <div className='pagination_wrapper'>
            {
                totalPages > 1 ? 
                <div className='pagination'>
                    { 
                        currentPage > 1 ?
                        <Link
                            to={__composePageLink(location, currentPage - 1)} 
                            className='page page_first'
                        >
                            Previous
                        </Link>
                        : 
                        null
                    }
                    {
                        __generatePages(currentPage, totalPages).map(page => (
                            <Link
                                key={page}
                                className={`page ${currentPage === page ? 'active': ''}`}
                                to={__composePageLink(location, page)} 
                            >
                                {page}
                            </Link>
                        ))
                    }
                    {
                        currentPage !== totalPages ?
                        <Link  
                            to={__composePageLink(location, currentPage + 1)}
                            className='page page_last'
                        >
                            Next
                        </Link>
                        :
                        null
                    }
                </div>
                : null
            }
        </div>
    )
}

export default pagination

const __generatePages = (current, total) => {
    // Try to build pages line
    let pages = [], result = [];
    for (let i=current-5; i<current+5; i++) {
        pages.push(i)
    }

    // Filter only valid values
    // and count possible shifted pages
    let leftPossiblePages = 0, rightPossiblePages = 0
    for (let i=0; i<pages.length; i++) {
        // if (pages[i] >0 && pages[i] <= total) 
        //     result.push(pages[i])
        if (pages[i] <= 0) {
            rightPossiblePages++
        }
        else if (pages[i] > total) {
            leftPossiblePages++
        }
        else {
            result.push(pages[i])
        }
    }

    // Try to restore shifted pages
    let minValidPage = pages[0]
    for (let i=0; i<leftPossiblePages; i++) {
        if (--minValidPage >= 0) {
            result.unshift(minValidPage)
        }
    }
    let maxValidPage = pages[pages.length - 1]
    for (let i=0; i<rightPossiblePages; i++) {
        if (++maxValidPage <= total) {
            result.push(maxValidPage)
        }
    }

    return result
}

const __composePageLink = (location, page) => {
    let params = new URLSearchParams(location.search)
    params.set('p', page)
    return `${location.pathname}?${params}`
}