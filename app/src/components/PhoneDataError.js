import React from 'react'

const PhoneDataErrorComponent = () => (
    <div className='data_table empty'>
            Sorry, there's no records for you :(
    </div>
)

export default PhoneDataErrorComponent