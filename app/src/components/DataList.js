import React from 'react'

const dataList = ({data, isFetching, onSaveClick, children, savedNumbers, messageIfSaved = 'remove from saved!'}) => {
    let pagination = null, error = null
    
    try {
        pagination = children[0]
        error = children[1]
    }
    catch (e) {
    }

    return (
        <div>
            { data.length > 0 || isFetching ?
            <div className={'data_table ' + (isFetching ? 'fetching' : '')}>
                {data.map((row, i) => (
                    <div key={row.id} className='data_row'>
                        <div className='data_cell'>
                            {row.name}
                        </div>
                        <div className='data_cell'>
                            {row.address}
                        </div>
                        <div className='data_cell data_cell_tels'>
                            {row.phoneNumbers.map(number=><span key={number} className='data_number'>{number}</span>)}
                        </div>
                        <div className='data_cell data_cell_save' onClick={() => onSaveClick(row)}>
                            {row.isFavorite 
                                ? messageIfSaved
                                : 'Save it!'}
                        </div>
                    </div>
                ))}
                {pagination}
            </div>
            : 
            error
            }
        </div>
    )
}

export default dataList