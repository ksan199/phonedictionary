import React from 'react'
import SavedPhoneDataList from '../containers/SavedPhoneDataList'
import SavedPhoneDataError from './SavedPhoneDataError'

const SavedNums = () => {
    return (
        <SavedPhoneDataList>
            {null}
            <SavedPhoneDataError />
        </SavedPhoneDataList>
    )
}

export default SavedNums