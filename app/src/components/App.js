import React from 'react';
import logo from './logo.svg';
import PhoneDataList from '../containers/PhoneDataList'
import SearchPannel from '../containers/SearchPannel'
import SavedPhoneDataList from '../containers/SavedPhoneDataList'
import Pagination from '../containers/PhoneDataPagination'
import { connect } from 'react-redux'
import { 
  fetchPhoneNumbers,
  fetchDefaultPhoneNumbers
} from '../actions'
import PhoneDataError from './PhoneDataError'
import SavedPhoneDataError from './SavedPhoneDataError'
import { Link, Route, Switch } from 'react-router-dom'
import SavedNums from './SavedNums'
import URLSearchParams from 'url-search-params'
import * as help from '../helpers'

class App extends React.Component {
  constructor(props) {
    super(props)

    this.backLink = '/'
    this.oldSearch
  }

  componentDidMount() {
    this.componentDidUpdate()
  }

  componentDidUpdate() {
    let searchQuery = (new URLSearchParams(this.props.location.search)).get('q'),
      page = (new URLSearchParams(this.props.location.search)).get('p'),
      path = this.props.location.pathname
    
    page = help.normalizePage(page)
    
    // Load nums each time we redirect to '/'
    if (path === '/') {
      // In case of default search fetching appropriate nums
      if (!searchQuery) {
        this.props.dispatch(fetchDefaultPhoneNumbers(page))
      }
      // Download data only on query change, show catched otherwise
      else if(this.oldSearch !== this.props.location.search) {
        this.props.dispatch(fetchPhoneNumbers(searchQuery, page))
      }
    }
  }

  componentWillUpdate() {
    this.backLink = `/${this.props.location.search}`

    // this.oldSearch serves previously requested query so we may use cached data
    if (this.props.location.pathname === '/') {
      this.oldSearch = this.props.location.search
    }
  }

  render () {
    return (
      <div className='App'>

        <div className='App-sidebar'>
          <div className='sidebar_title'>
            <h2>Saved nums</h2>
          </div>
          <SavedPhoneDataList>
            {null}
            <SavedPhoneDataError />
          </SavedPhoneDataList>
        </div>
        
        <div className='App-header'>
          <Link to='/'>
            <img src={logo} className='App-logo' alt='logo' />
          </Link>
          <h2>Powered by React</h2>
          <SearchPannel/>
        </div>
        
        <div className='links-list'>
          <Switch>
            <Route exact path='/'>
              <Link className='links-list_link' to='/saved'>
                <h2>Saved nums</h2>
              </Link>
            </Route>
            <Route path='/saved'>
              <Link className='links-list_link' to={this.backLink}>
                <h2>Back</h2>
              </Link>
            </Route>
          </Switch>
        </div>

        <Switch>
          <Route exact path='/'>
            <div>
              <div className='App-data'>
                <PhoneDataList messageIfSaved='Saved!'>
                  <Pagination />
                  <PhoneDataError />
                </PhoneDataList>
              </div>
            </div>
          </Route>
          <Route path='/saved'>
            <SavedNums />
          </Route>
          <Route>
            <h2>There is no such page, we are sorry.</h2>
          </Route>
        </Switch>
      </div>
    );
  }
}

export default connect()(App)
