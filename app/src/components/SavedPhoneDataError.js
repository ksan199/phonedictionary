import React from 'react'

const savedPhoneDataError = () => (
    <div className='data_table empty'>
        Select important number and click 'Save it!' in order to store it here
    </div>
)

export default savedPhoneDataError