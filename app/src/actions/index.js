import fetch from 'isomorphic-fetch'
import * as help from '../helpers'

export const RECORDS_PER_PAGE = 50
export const RECIEVE_PHONE_NUMBERS = 'RECIEVE_PHONE_NUMBERS'
export const RECIEVE_SAVED_NUMBERS = 'RECIEVE_SAVED_NUMBERS'
export const REQUEST_PHONE_NUMBERS = 'REQUEST_PHONE_NUMBERS'
export const ADD_SAVED_NUMBER = 'ADD_SAVED_NUMBER'
export const REMOVE_SAVED_NUMBER = 'REMOVE_SAVED_NUMBER'
export const SET_TOTAL_PAGES = 'SET_TOTAL_PAGES'
export const SET_FAVORITE = 'SET_FACORITE'

export const DevDomainUrl = 'http://localhost:5000/'

let fetchPhoneNumbersUrl = 'api/nums'
let fetchDefaultPhoneNumbersUrl = 'api/nums/allpersons'
let domain = ''

if (process.env.NODE_ENV === 'development') {
    domain = DevDomainUrl
}

fetchDefaultPhoneNumbersUrl = domain.concat(fetchDefaultPhoneNumbersUrl)
fetchPhoneNumbersUrl = domain.concat(fetchPhoneNumbersUrl)

export const fetchDefaultPhoneNumbers = (page)  => {
    return (dispatch, getState) => {
        dispatch(requestPhoneNumbers())
        
        let url = encodeURI(`${ fetchDefaultPhoneNumbersUrl }?skip=${ (page - 1)*RECORDS_PER_PAGE }&take=${ RECORDS_PER_PAGE }`)
        let savedNums = getState().savedSet

        fetch(url)
            .then(res => res.json())
            .then(data => {
                data.persons = data.persons.map(p => {
                    return {
                        id: p.personId,
                        name: `${p.firstName} ${p.lastName}`,
                        address: p.address,
                        phoneNumbers: p.nums.map(n => n.number),
                        // Check if this number's stored in set of favorites
                        isFavorite: __ckeckIfSaved(savedNums, p.personId)
                    }
                })
                dispatch(recievePhoneNumbers(data.persons, data.totalRecords))
            })
            .catch(e => {
                dispatch(recievePhoneNumbers([], 0))
            })
    }
}

export const fetchPhoneNumbers = (searchQuery, page)  => {
    return (dispatch, getState) => {
        dispatch(requestPhoneNumbers())
        
        let url = encodeURI(`${ fetchPhoneNumbersUrl }?q=${ searchQuery }&skip=${ (page - 1)*RECORDS_PER_PAGE }&take=${ RECORDS_PER_PAGE }`)
        let savedNums = getState().savedSet

        fetch(url)
            .then(res => res.json())
            .then(data => {
                data.persons = data.persons.map(p => {
                    return {
                        id: p.personId,
                        name: `${p.firstName} ${p.lastName}`,
                        address: p.address,
                        phoneNumbers: p.nums.map(p => p.number),
                        // Check if this number's stored in set of favorites
                        isFavorite: __ckeckIfSaved(savedNums, p.personId)
                    }
                })
                dispatch(recievePhoneNumbers(data.persons, data.totalRecords))
            })
            .catch(e => {
                dispatch(recievePhoneNumbers([], 0))
            })
    }
}

const __ckeckIfSaved = (savedNums, id) => {
    let commons = savedNums.filter(num => num.id === id)
    return commons.length === 0 
        ? false
        : true
}

export const recievePhoneNumbers = (data, totalRecords) => {
    return {
        type: RECIEVE_PHONE_NUMBERS,
        data,
        totalRecords
    }
}

export const recieveSavedNumbers = () => {
    return {
        type: RECIEVE_SAVED_NUMBERS
    }
}

export const requestPhoneNumbers = () => {
    return {
        type: REQUEST_PHONE_NUMBERS
    }
}

export const addSavedNumber = number => {
    return {
        type: ADD_SAVED_NUMBER,
        number
    }
}

export const removeSavedNumber = number => {
    return {
        type: REMOVE_SAVED_NUMBER,
        number
    }
}

export const removeNumber = number => {
    return dispatch => {
        // Remove from localStore
        let ls = help.getObjectFromLS('savedSet')
        ls = ls.filter(tel => tel.id !== number.id)
        help.putObjectToLS('savedSet', ls)
        // Remove from redux store
        dispatch(removeSavedNumber(number))
        // Reflect changes to result set array
        dispatch(setFavorite(number.id, false))
    }
}

export const saveNumber = number => {
    return dispatch => {
        number = {...number, isFavorite: true}
        // Trying to add to localStore
        let ls = help.getObjectFromLS('savedSet')
        // Doing nothing if pushing duplicate
        for (let i=0; i<ls.length;i++) {
            if (ls[i].id === number.id) {
                return
            }
        }
        ls.push(number)
        help.putObjectToLS('savedSet', ls)
        // Add entity to redux store in order to rerender UI
        dispatch(recieveSavedNumbers())
        // Reflect changes to result set array
        dispatch(setFavorite(number.id, true))
    }
}

export const setFavorite = (id, value) => {
    return {
        type: SET_FAVORITE,
        id,
        value
    }
}

export const setTotalPages = pages => {
    return {
        type: SET_TOTAL_PAGES,
        pages
    }
}


  
