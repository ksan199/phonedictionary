import { connect } from 'react-redux'
import PhoneDataList from '../components/DataList'
import { saveNumber } from '../actions'

const mapStateToProps = (state, ownProps) => {
    return {
        data: state.resultSet.data,
        isFetching: state.resultSet.isFetching,
        savedNumbers: state.savedSet,
        messageIfSaved: ownProps.messageIfSaved
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSaveClick: number => {
            dispatch(saveNumber(number))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PhoneDataList)