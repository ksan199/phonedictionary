import Pagination from '../components/Pagination'
import { connect } from 'react-redux'
import { fetchPhoneNumbers } from '../actions'
import { withRouter } from 'react-router-dom'

const mapStateToProps = (state, ownProps) => {
    return {
        totalPages: state.resultSet.totalPages
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPageClick: (qstring, page) => {
            dispatch(fetchPhoneNumbers(qstring, page))
        }
    }
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Pagination))