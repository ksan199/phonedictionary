import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import URLSearchParams from 'url-search-params'

const SearchPannel = ({dispatch, history, location}) => {
    let input

    return (
        <div className='search_pannel'>
            <form className='search_form' onSubmit={(e) => {
                e.preventDefault()
                input.blur()
                if (!input.value.trim()) {
                    history.push('')
                }
                
                let params = new URLSearchParams()
                params.set('q', input.value.trim())
                params.set('p', 1)
                history.push(`/?${params}` )
            }}
            >  
                <input className='search_input' ref={(node)=> {
                    input = node
                }} 
                autoFocus placeholder='Input first name, last name or address'/>
            </form>
        </div>
    )
}

export default withRouter(connect()(SearchPannel))
