import { connect } from 'react-redux'
import PhoneDataList from '../components/DataList'
import { removeNumber } from '../actions'

const mapStateToProps = state => {
    return { 
        data: state.savedSet
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSaveClick: number => {
            dispatch(removeNumber(number))
        }
    }
}

export default connect(
    mapStateToProps, 
    mapDispatchToProps
)(PhoneDataList)