export const getObjectFromLS = key => {
    // Does value exists? return [] if it doesn't or if its value is empty string
    return localStorage.getItem(key) 
        ? localStorage.getItem(key) === ''
            ? []
            : JSON.parse(localStorage.getItem(key))
        : []
}

export const putObjectToLS = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value))
}

export const normalizePage = page => {
    page = parseInt(page)
    if (!page)
        page = 1
    page = Math.max(1, page)
    page = Math.floor(page)
    return page
}