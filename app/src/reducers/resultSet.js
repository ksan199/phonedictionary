import { 
    RECIEVE_PHONE_NUMBERS, 
    REQUEST_PHONE_NUMBERS,
    SET_TOTAL_PAGES,
    RECORDS_PER_PAGE,
    SET_FAVORITE
} from '../actions'

export default function(
    state = { 
        isFetching: false, 
        data: [],
        totalPages: 1
    }, 
    action
) {
    switch (action.type) {
        case RECIEVE_PHONE_NUMBERS:
            return {
                ...state,
                isFetching: false,
                data: action.data,
                totalPages: Math.ceil(action.totalRecords / RECORDS_PER_PAGE)
            }
        case REQUEST_PHONE_NUMBERS:
            return {
                ...state,
                isFetching: true
            }
        case SET_TOTAL_PAGES:
            return {
                ...state,
                totalPages: action.pages
            }
        case SET_FAVORITE: 
            return {
                ...state,
                // Set favorite value for selected number
                data: state.data.map(number => {
                    if (number.id === action.id) {
                        number.isFavorite = action.value
                    }
                    return number
                })
            }
        default:
            return state
    }
}