import { combineReducers } from 'redux'
import resultSet from './resultSet'
import savedSet from './savedSet'

const app = combineReducers({
    resultSet,
    savedSet
})

export default app