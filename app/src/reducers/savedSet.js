import { 
    RECIEVE_SAVED_NUMBERS,
    ADD_SAVED_NUMBER,
    REMOVE_SAVED_NUMBER
 } from '../actions'
import * as help from '../helpers'

export default function(state = [], action){
    switch (action.type) {
        case RECIEVE_SAVED_NUMBERS: {
            return help.getObjectFromLS('savedSet')
        }
        case ADD_SAVED_NUMBER: {
            return [
                ...state,
                action.number
            ]
        }
        case REMOVE_SAVED_NUMBER: {
            return state.filter(tel => tel.id !== action.number.id)
        }
        default:
            return state
    }
}